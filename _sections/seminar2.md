---
title: Term 2
icon: fa-th
order: 4
---

{% for course in site.seminar2 %}
 [{{course.title}}]({{course.url|relative_url}})
{% endfor %}
