---
layou: "page"
title: Documentation Guidelines
icon: fa-file-signature
order: 6
---

# **Documentation for the Master in Design for Emergent Futures:**


![document please](https://media.giphy.com/media/xT4uQwLt2AyurOGWFW/giphy.gif)

## **TERM 1**

Documentation will be reviewed every following Monday after the bootcamp takes place. The documentation will be reviewed by Master directors and coordinators, as well as by the tutors responsible for the specific seminar or workshop.

How should you document your work? 

1- Include videos, images, animations, or any other form of representation to support your written documentation. Every documentation piece should contain, at least, one featured image/animation/video. Apart from the featured visual resources, others could be added to the other parts of the documentation described below.  
2- You are required to write a post of at least 500 words responding the following questions:  
  · What did you do that is new. What did you learn?   
  · How can your learning process be described? please use bullet points (minimum 5)  
  · How is this relevant to your work?  
3- You could add additional information and resources to better explain and represent your process and experience. Each seminar/workshop might include additional documentation requirements. 
4- Once your documentation is ready, commit your changes in GitLab  
5- After changes are committed, go to the preview page and print a PDF to be saved in your computer  
6- Once you save the PDF, go the *Name of the Course // SUBMISSIONS* shared folder in Google Drive and create a folder with your name inside  
7- Save the PDF in the folder you just created with your name  
8- You are Done!  