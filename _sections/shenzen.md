---
title: Research Trip
icon: fa-th
order: 3
---

{% for course in site.data.shenzen %}
 [{{course.name}}]({{course.url|relative_url}})
{% endfor %}
