---
title: Designing with Extended Intelligence
layout: "page"
order: 06
---


![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/6b55083f13fe-Joler.jpg)

### Faculty
Ramon Sangüesa  
Lucas Peña 

### Syllabus and Learning Objectives
The course explores the development of artificial intelligence and its close connection with design from its very beginning.
We will delve into the concepts of design that exist in AI and will connect them with their implications and their possibilities as guidelines for emergent design.
In this process we will explore: the autonomization of the object, the collective dimension of intelligent behaviour and the challenges that they pose for established design methods.  

### Presentations from Tutors  

<iframe src="https://drive.google.com/file/d/1KHy1pTuLm_D1lgtlQiShOgmnH9C650Rk/preview" width="700" height="450"></iframe>  

### Total Duration
Classes: Mon-Fri (3-6pm) (15 per week)  
Student work hours: 12 per week  
Total hours per student: 27 per week  

### Structure and Phases
1. Artificial intelligence as an evolving field  
1.1 Historical perspective  
1.2 Concepts of intelligence in AI  
1.1 Operationalization of intelligence  
1.3 Intelligence as Design  
1.4 Exploration project: questioning intelligence  

2. AI Fields  
2.1 Cognitive Tasks  
2.2 Tasks and Techniques  
2.3 From Classification to Construction  
2.4 The learning component  
2.4.1 The current canonic view: ML and Big Data   
2.4.2 Beyond the ML and Big Data paradigm  
2.4 Exploration project: questioning ML and Big Data  

3. Collective AI  
3.1 Mind as a Collective  
3.2 Agents and Multiagents  
3.3 Users as actors and agents  
3.4 Hybrid Intelligent Networks: from Metaobjects to Chaotic Systems  
3.5 Exploration project: networked AI design  

4. The economics and societal impact of AI  
4.1 Accelerated intelligence in capitalism  
4.2 Connecting business  
4.3 AI as an amplifier: from bias to discrimination  
4.4 AI, ethics and politics: manifestos and laws  
4.4 Exploration project: privilege and AI  

5. Towards a methodology for AI design  
5.1 Designing for AI  
5.2 Desigining with AI  
5.3 Exploration project: build your design toolbox   

### Output
The format of your work is quite open. Use the one that feel comfortable to express what you need to share: text, video, software, object, performance... 

### Grading Method
Individual work and presentation: 65%  
Quality of documentation (sources, argumentation, coherence): 35%  

### Bibliography
[A PROPOSAL FOR THE DARTMOUTH SUMMER RESEARCH PROJECT ON ARTIFICIAL INTELLIGENCE](http://web.archive.org/web/20170507180753/http://www-formal.stanford.edu/jmc/history/dartmouth/)  
[Stuart Russell and Peter Norving. Artificial Intelligence, a Modern Approach.](http://aima.cs.berkeley.edu/)  
[An Open Letter: Research Priorities for Robust and Beneficial Artificial Intelligence](https://futureoflife.org/ai-open-letter/)  
[Barcelona declaration for the proper development and usage of Artificial Intelligence in Europe](http://www.bdebate.org/sites/default/files/barcelona-declaration_v7-1-eng.pdf)  
[The 23 principles for AI of the Asilomar conference](https://futureoflife.org/ai-principles)  
Pasquale, Frank. 2015. The Black Box Society: The Secret Algorithms That Control Money and Information. Cambridge: Harvard University Press.  
Cathy O’Neill Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy. Crown Publishers  
[Joi Ito, Director of the MIT Media Lab on "Challenges of Extended Intelligence"](https://www.youtube.com/watch?v=KF9ZqnEiSzU)    
[Panel in World Economic Foum (Davos, 2017) on Artificial Intelligence with Microsoft CEO Nadella, IBM CEO Rometty and the director of the MIT Media Lab](https://thischangeseverything.org/book/)  
[Trends: the stanford study on AI 100 years](https://ai100.stanford.edu/) Long-term research on the development and impact of AI.    
[AI & Ethics](https://www.amazon.com/gp/product/0262720477?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s01)  
Luis Suarez-Villa, Technocapitalism: A Critical Perspective on Technological Innovation and Corporatism (Philadelphia: Temple University Press, 2009).  
Tiqqun. The Cybernetic Hypothesis. (2014). Accessed 15 Sept. 2017. 
[Richard Babbrook, “The Californian Ideology”](http://www.imaginaryfutures.net/2007/04/17/the-californian-ideology-2/)  
Adam Curtis. All Watched Over by Machines of Loving Grace (TV series)  
[Vladan Joler. Anatomy of an AI System.](https://anatomyof.ai/)  
Sara Córdoba, Wimer Hazenberg, Menno Huisman. (2011). Metaproducts. Meaningful Design For Our Connected World B/SPublishers  
M Kuniavski (2010). Smart Things: Ubiquitous Computing User Experience Design. Morgan Kaufmann. 
Frank Pasquale The Algorithmic Self (2015). The Hedgehog Review: Vol. 17 No. 1  
Adam Greenfield (2017). Radical Technologies  

### Background Research Material
Herbert Simon. The Sciences of the Artificial. 
Rodney Brook. Intelligence without Reason.  
Hubert Dreyfuss. What Computers Can't Do.  
Marvin Minsky. The Society of Mind. 
[Superintelligence: Paths Dangers, Strategies](https://www.amazon.com/Superintelligence-Dangers-Strategies-Nick-Bostrom/dp/1501227742)
[Grady Booch: Don't fear superintelligent AI](https://www.ted.com/talks/grady_booch_don_t_fear_superintelligence?language=en)  
[Jeremy Howard: The wonderful and terrifying implications of computers that can learn](https://www.ted.com/talks/)  
[Shyam Sankar: The rise of human-computer cooperation](https://www.ted.com/talks/shyam_sankar_the_rise_of_human_computer_cooperation/)  
[Breaking Walls Conference Berlin, 2016. BREAKING THE WALL TO LIVING ROBOTS. How Artificial Intelligence Research Tries to Build Intelligent Autonomous Systems](http://falling-walls.com/videos/Luc-Steels-1648)  
[The EDGE questions. 2015 : WHAT DO YOU THINK ABOUT MACHINES THAT THINK?](https://www.edge.org/responses/q2015)  
Jennifer Robertson. Robo Sapiens Japanicus: Robots, Gender, Family, and the Japanese Nation  
Laura Forlano. Posthumanism and Design. 

### Requirements for the Students
Be open and curious, question the current views on AI and Design and go forward from there (... and tinker a little with BigML machine learning online software platform)

### Infrastructure Needs
Projector, wifi, room space, some walls to post partial results of the projects going on at the course.

### RAMON SANGÜESA
![image](https://gitlab.com/MDEF/landing/raw/master/05425adc4b62-CV_FOTO_RamonSanguesa_S.jpg)

### Email Address
<rsanguesa@elisava.net>

### Personal Website
[equipocafeína](http://equipocafeina.net)

### Twitter Account
@ramonsang

### Professional BIO
Ramon Sangüesa has been doing research in Artificial Intelligence for more than 20 years specializing in Machine Learning, Uncertain Reasoning and Multiagent Systems. He is a professor at the Technical University of Catalonia (on academic leave) and now works as the Head of Technology Research at the Elisava Design and Engineering School. He has been visiting professor and researcher at Columbia University in NYC and Toronto University. He has been involved in Digital Social Innovation since long, being one of the creators of Citilab (Citizen's lab) in Barcelona and his first director of innovation. 